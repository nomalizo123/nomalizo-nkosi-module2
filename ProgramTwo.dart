// This program will take in Award Winning Apps

void main() {
  List<String> awardWinningApps = [
    'Plascon Visualiser App',
    'Dstv App',
    'Supersports App',
    'M4Jam SA App',
    'Tutu App',
    'Shyft App',
    'Ctrl App',
    'Digger App',
    'Checkers Sixty60 App',
    'iiDENTIFii App'
  ];

//Sorting the apps
  awardWinningApps.sort();

//Printing the apps by names
  print(awardWinningApps);

//Prints out the 2017 app after the list has been sorted
  print("The 2017 Award winning App: " + awardWinningApps[5]);

//Prints out the 2018 app after the list has been sorted
  print("The 2018 Award winning App: " + awardWinningApps[6]);

//Print total number of apps in the array
  print(awardWinningApps.length);
}
