//program number 3
void main() {
  BusinessApp obj =
      new BusinessApp("name", "category", "developer", DateTime.monthsPerYear);

  obj.display();
}

class BusinessApp {
  var name = "Ambani frica";
  var category = "Educational Product";
  var developer = "Mukundi Lambani";
  var year = 2021;

  BusinessApp(String name, String category, String developer, int year) {
    this.name = name;
    this.category = category;
    this.developer = developer;
    this.year = year;
  }
  void display() {
    print(name.toUpperCase());
    print("The category of the app is: $category");
    print("The developer of the app is: $developer");
    print("The year of when the app won is: $year");
  }
}
